

function countLetter(letter, sentence) {
    let result = 0;

    function checkLetter(letter){
        if(typeof letter == 'string') {
            if(letter.length === 1) {

               const count = sentence.split(`${letter}`).length - 1;
               console.log(count);
               return count;


            } else {
               return undefined;
            }


        } else {
            return undefined;
        }
    } 

    return checkLetter(letter);


    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    let charRepeats = function(str) {
        for (let i=0; i<str.length; i++) {
          if ( str.indexOf(str[i]) !== str.lastIndexOf(str[i]) ) {
            return false; // repeats
          }
        }
        return true;
    }

    return charRepeats(text);
   
}



function purchase(age, price) {
    // 
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
   
     function roundOff(age, price) {
           if(age < 13) {
            return undefined; 
           } else if ( age >= 13 && age <= 21) {
            return price = toString(0.8*price); 
           } else  if(age >= 22 && age <= 64){
            return toString(price); 
           } else {
            return price = toString(0.8*price); 
           }
     }  

    //  if(number(age) < 13) {
    //     console.log  ('undefined'); 
    //  }

    //  if(number(age) >= 13 && number(age) <= 21) {
    //    console.log (price = toString(round(0.8*price)));
    // }

    
    return roundOff(age, price);

   
  
     // return roundOff(age, price);
    
}




function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:

    const item = [
        { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
        { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
        { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
        { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
        { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

        ]

    // let checkCat = () =>  {

    //     for (i = 0, i < item.length, i++ ){

    //     }
    // }

    item[2], item[3], item[4]

    return checkCat();


    // console.log(item[0]);

    // for (i=0, i < item.length, i++) {
    //    if (item[i].stocks === 0) {
    //         console.log("YY")
    //    }
    // }



    // return ['toiletries', 'gadgets'];
    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.



}



function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    let arr1 =  ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    let arr2 = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
    function arrayMatch(arr1, arr2) {
      var arr = [];  // Array to contain match elements
      for(var i=0 ; i<arr1.length ; ++i) {
        for(var j=0 ; j<arr2.length ; ++j) {
          if(arr1[i] == arr2[j]) {    // If element is in both the arrays
            arr.push(arr1[i]);        // Push to arr array
          }
        }
      }
       
      return arr;  // Return the arr elements
    }
   

    // function checkVoter(candidateA, candidateB) {
    //     const intersection = candidateA.filter(element => candidateB.includes(element));
    //     console.log(intersection);

    //     // return ['LIWf1l', 'V2hjZH'];
    // }


    return arrayMatch(arr1, arr2);

}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};